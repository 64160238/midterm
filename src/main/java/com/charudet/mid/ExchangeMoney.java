package com.charudet.mid;

import java.util.Scanner;

public class ExchangeMoney {
	private String name;
	private int num;

	public ExchangeMoney(String name, int num) {
		this.name = name;
		this.num = num;
	}

	public boolean exchange(int num) {
		if (num >= 1000) {
			System.out.println("1000 Bath = " + num / 1000);
			num = num % 1000;
		}

		if (num >= 500) {
			System.out.println("500 Bath = " + num / 500);
			num = num % 500;
		}

		if (num >= 100) {
			System.out.println("100 Bath = " + num / 100);
			num = num % 100;
		}

		if (num >= 100) {
			System.out.println("100 Bath = " + num / 100);
			num = num % 100;
		}

		if (num >= 50) {
			System.out.println("50 Bath = " + num / 50);
			num = num % 50;
		}

		if (num >= 20) {
			System.out.println("20 Bath = " + num / 20);
			num = num % 20;
		}

		if (num >= 10) {
			System.out.println("10 Bath = " + num / 10);
			num = num % 10;
		}

		if (num >= 5) {
			System.out.println("5 Bath = " + num / 5);
			num = num % 5;
		}

		if (num >= 2) {
			System.out.println("2 Bath = " + num / 2);
			num = num % 2;
		}

		if (num >= 1) {
			System.out.println("1 Bath = " + num / 1);
			num = num % 1;
		}
		return true;
	}

	public void print() {
		System.out.println(name +" "+ name);
	}

	public String getName() {
		return name;
	}

	public int getNum() {
		return num;
	}
}
