package com.charudet.mid;


public class MultiplicationTable {
	private int x;
	private String name;

	public MultiplicationTable(String name, int x) {
		this.x = x;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public int getX() {
		return x;
	}

	public void printX() {
		for (int i = 1; i <= 12; i++) {
			System.out.println(x + " * " + i + " = " + (i * x));
		}
	}
}
